#include "functions.h"
#include <iostream>

class Student {
public:
	int _age;

	Student(int age) {
		_age = age;
	}

	friend std::ostream & operator<<(std::ostream & l, Student student)
	{
		l << student._age;
		return l;
	}

	bool operator <(const Student &b) const {
		return _age < b._age;
	}

	bool operator >(const Student &b) const {
		return _age > b._age;
	}

	bool operator ==(const Student &b) const {
		return _age == b._age;
	}

};

template<class T>
void runTest(T arr[], int size, T compareArray[5]) {
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<T>(compareArray[0], compareArray[1]) << std::endl;
	std::cout << compare<T>(compareArray[2], compareArray[3]) << std::endl;
	std::cout << compare<T>(compareArray[4], compareArray[4]) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	bubbleSort<T>(arr, size);
	for (int i = 0; i < size; i++) {
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<T>(arr, size);
	std::cout << std::endl;
}

int main() {

	double doubleArray[5] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	double compareDoubleArray[5] = { 1.0, 2.5, 4.5, 2.4, 4.4};
	runTest<double>(doubleArray, 5, compareDoubleArray);

	char charArray[5] = { 'D', 'B', 'E', 'A', 'C' };
	char compareCharArray[5] = { 'A', 'C', 'D', 'B', 'E' };
	runTest<char>(charArray, 5, compareCharArray);

	Student studentArray[5] = { Student(13), Student(14), Student(15), Student(16), Student(17) };
	Student compareStudentArray[5] = { Student(1), Student(2), Student(4), Student(2), Student(4) };
	runTest<Student>(studentArray, 5, compareStudentArray);

	system("pause");
	return 1;
}