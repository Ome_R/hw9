#pragma once

#include <iostream>

template<class T>
int compare(T first, T second) {
	return first < second ? 1 : first == second ? 0 : -1;
}

template<class T>
void swap(T& first, T& second) {
	T temp = first;
	first = second;
	second = temp;
}

template<class L>
void bubbleSort(L arr[], int size) {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size - i - 1; j++) {
			//Checking if the first index is bigger than the one after it.
			if (compare<L>(arr[j], arr[j + 1]) == -1) {
				swap<L>(arr[j], arr[j + 1]);
			}
		}
	}
}

template<class L>
void printArray(L arr[], int size) {
	for (int i = 0; i < size; i++) {
		std::cout << arr[i] << std::endl;
	}
}

