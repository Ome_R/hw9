#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
using namespace std;


int main()
{
	BSNode<string>* stringTree = new BSNode<string>("1");
	stringTree->insert("2");
	stringTree->insert("3");
	stringTree->insert("4");
	stringTree->insert("5");
	stringTree->insert("1");
	stringTree->insert("2");
	stringTree->insert("3");
	stringTree->insert("4");
	stringTree->insert("5");

	stringTree->printNodes();

	BSNode<int>* intTree = new BSNode<int>(1);
	intTree->insert(2);
	intTree->insert(3);
	intTree->insert(4);
	intTree->insert(5);
	intTree->insert(1);
	intTree->insert(2);
	intTree->insert(3);
	intTree->insert(4);
	intTree->insert(5);

	stringTree->printNodes();

	BSNode<string>* bs = new BSNode<string>("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");

	
	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 2 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;

	system("pause");
	delete bs;

	return 0;
}

