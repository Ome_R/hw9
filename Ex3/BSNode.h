#ifndef BSNode_H
#define BSNode_H

#include <iostream>
#include <string>

using namespace std;

template<class T>
class BSNode
{
public:
	BSNode(T data) {
		_data = data;
		_count = 1;
		_left = nullptr;
		_right = nullptr;
	}

	BSNode(const BSNode<T>& other) {
		_data = other._data;
		_count = other._count;
		_left = other._left;
		_right = other._right;
	}

	~BSNode() {
		if (_right != nullptr) {
			delete _right;
		}
		if (_left != nullptr) {
			delete _left;
		}
	}
	
	void insert(T value) {
		if (_data == value)
			_count++;
		else {
			if (_data < value) {
				if (_right == nullptr)
					_right = new BSNode(value);
				else
					_right->insert(value);
			}
			else {
				if (_left == nullptr)
					_left = new BSNode(value);
				else
					_left->insert(value);
			}
		}
	}

	BSNode& operator=(const BSNode<T>& other) {
		BSNode* node = new BSNode(other);
		return *node;
	}

	bool isLeaf() const {
		return _left == nullptr && _right == nullptr;
	}

	T getData() const {
		return _data;
	}

	BSNode* getLeft() const {
		return _left;
	}

	BSNode* getRight() const {
		return _right;
	}

	bool search(T val) const {
		if (_data > val && _right != nullptr)
			return _right->search(val);
		else if (_data < val && _left != nullptr)
			return _left->search(val);
		else
			return val == _data;
	}

	int getHeight() const {
		int leftHeight = 1, rightHeight = 1;

		if (_left != nullptr)
			leftHeight += _left->getHeight();
		if (_right != nullptr)
			rightHeight += _right->getHeight();

		return leftHeight > rightHeight ? leftHeight : rightHeight;
	}

	int getDepth(const BSNode& root) const {
		return root.getHeight() - getHeight();
	}

	void printNodes() const {
		if (_left != nullptr)
			_left->printNodes();

		std::cout << _data;
		if (_count != 1)
			std::cout << " (" << _count << ")";
		std::cout << std::endl;

		if (_right != nullptr)
			_right->printNodes();
	}

private:
	T _data;
	BSNode* _left;
	BSNode* _right;
	int _count;

};

#endif